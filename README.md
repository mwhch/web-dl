# web-interface for making private-copies of media files

Webinterface to download files into the webserver. The idea is to save downloaded files into your media-server library or shared folder so it is accessible from connected devices.

## Requirements:
 - youtube-dl
 - apache2
 - php

    Installation one-liner:
     * **debian based**:
    
            sudo apt-get -y install youtube-dl apache2 php

## Installation:
1. download the git repository:

        git clone https://gitlab.com/mwhch/web-dl.git

2. move the contained files to your public-html directory (most commonly `/var/www/html/`)    
        
        sudo cp -r web-dl /var/www/html/

3. [optionally] configure the download path
   
   the download path is specified within the `download.php` file. To edit this path, change the value of `$path` and optionally `$music_path` and `$video_path`. 
   The files are being uploaded to `$path` and based upon the format decision (audio/video) the `$audio_path` or `$video_path` suffix is added to this path. To do this, simply edit the file for example with

        sudo nano download.php

   and locate the lines where the above mentioned variables are set. 

   
## Troubleshooting:
  * ### file does not download:
    
    - **Make sure you have sufficient rights** to write in the folder you are     downloading into. 
    In particular, user www-data needs writing rights in order to be able to save the files. You can achieve this by adding a new downloaders-group, add the www-data user to this group and gain writing access over the destination folder to this group. This can be done like the following:
      -  first let's add a new group called web-downloaders:
    
             sudo groupadd web-downloaders

      -  now we add the user www-data to our new group (when downloading via the webinterface, user www-data is acessing your storage which is only possible with suficient permissions):
            
             sudo usermod -a -G web-downloaders www-data
    
      - in the last remaining step we set our new group as the owner of the directory and adjust the permissions:
            
            sudo chgrp web-downloaders $DOWNLOADS
        where `$DOWNLOADS` is your download directory which is located by default within the web-dl folder (so the default command would look something like `sudo chgrp web-downloaders downloads/`) unless you changed something in step 3.
        
        To adjust the permisisons run this command

            sudo chown -R g+rw $DOWNLOADS

    